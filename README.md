# Renovate
Renovate is used by Digital Lab to automatically propose dependency updates. The bot is able to track a wide range of dependencies from various sources, and will create Merge requests for them. It will also automatically maintain and update the merge requests.

# Prerequisites
- GitLab runner

# Usage
Renovate runs in a CI pipeline. It will automatically run once per day. Furthermore, any commit made to any branch will trigger a dry-run, which will allow the committer to see what the bot would do with the changes without actually creating the merge requests. The bot is configured through `config.yaml` for global settings. Settings can also be configured on a per-repository basis (https://docs.renovatebot.com/configuration-options/).

# Configuration
## Maven registries
Renovate can automatically detect maven registries. Unfortunately this process is relatively unstable and many dependencies get missed due to not finding the registry source. As such, we manually provide renovate with a set of registries to always look through for every repository. This means that automatic registry detection is completely disabled. New registries can be added in `config.yaml` under `packageRules.registryUrls`.

## Namespaces
The bot is manually configured to only look in certain groups of the Digital Lab GitLab group. This can be configred in `config.yaml` at `autodiscoverNamespaces`.